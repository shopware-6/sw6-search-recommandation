<?php
declare(strict_types=1);

namespace JMSE\SearchRecommendation\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1705777260CreateNewTable extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1705777260;
    }

    public function update(Connection $connection): void
    {
        $query = <<<SQL
        CREATE TABLE IF NOT EXISTS `search_recommandation` (
          `id` MEDIUMINT NOT NULL AUTO_INCREMENT,
          `query` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL UNIQUE,
          `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
          `published` BIT NOT NULL,
          `created_at` datetime(3) DEFAULT NULL,
          `updated_at` datetime(3) DEFAULT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        SQL;

        $connection->executeStatement($query);
    }

    public function updateDestructive(Connection $connection): void
    {
    }
}

