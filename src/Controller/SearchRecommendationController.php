<?php declare(strict_types=1);

namespace JMSE\SearchRecommendation\Controller;

use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\Routing\Annotation\Route;

use Shopware\Core\Framework\Context;

/**
 * @Route(defaults={"_routeScope"={"store-api"}})
 */
class SearchRecommendationController extends StorefrontController
{
    /**
     * @Route("/store-api/search/recommandation/{search}", name="store-api.search.recommandation", methods={"GET"})
     */
    public function recommandation(string $search)
    {
        // Command for Training with Category (name),Product (sku,name) and Manu (name)
        // Custom Publisher Review for user suggests ? In Frontend Admin View ?
        // Migration for Table Index

        $connection = \Shopware\Core\Kernel::getConnection();
        $connection->insert('search_recommandation',['type'=> 'generated','query'=> $search]);

        return $this->json(['message' => 'it works']);
    }
}