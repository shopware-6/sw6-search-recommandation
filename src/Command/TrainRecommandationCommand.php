<?php declare(strict_types=1);

namespace JMSE\SearchRecommendation\Command;

use Doctrine\DBAL\Driver\Mysqli\Driver;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\DBAL\Connection;
use Symfony\Component\DependencyInjection\Container;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

class TrainRecommandationCommand extends Command
{
    protected static $defaultName = 'jmse:train:reco';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $connection = \Shopware\Core\Kernel::getConnection();
        $connection->executeStatement("DELETE FROM search_recommandation WHERE type='generated'");

        $p = $connection->fetchAllAssociative('SELECT * FROM product_translation');

        foreach ($p as $key => $product) {
            $count = $connection->fetchAllAssociative('SELECT * FROM search_recommandation WHERE query = "'.$product['name'].'"');

            if(count($count) === 0){
                $connection->insert('search_recommandation', ['query' => $product['name'], 'type' => 'generated', 'count'=> 100]);
            }
        }

        $pn = $connection->fetchAllAssociative('SELECT product_number FROM product');

        foreach ($pn as $key => $product) {
            $count = $connection->fetchAllAssociative('SELECT * FROM search_recommandation WHERE query = "'.$product['product_number'].'"');

            if(count($count) === 0){
                $connection->insert('search_recommandation', ['query' => $product['product_number'], 'type' => 'generated', 'count'=> 100]);
            }
        }

        $c = $connection->fetchAllAssociative('SELECT * FROM category_translation');

        foreach ($c as $key => $category) {
            $count = $connection->fetchAllAssociative('SELECT * FROM search_recommandation WHERE query = "'.$category["name"].'"');

            if(count($count) === 0){
                $connection->insert('search_recommandation', ['query' => $category['name'], 'type' => 'generated', 'count'=> 100]);
            }
        }

        $m = $connection->fetchAllAssociative('SELECT * FROM product_manufacturer_translation');

        foreach ($m as $key => $category) {
            $count = $connection->fetchAllAssociative('SELECT * FROM search_recommandation WHERE query = "'.$category["name"].'"');

            if(count($count) === 0){
                $connection->insert('search_recommandation', ['query' => $category['name'], 'type' => 'generated', 'count'=> 100]);
            }
        }

        $output->writeln('Train Recommandation ...');
        return self::SUCCESS;
    }
}